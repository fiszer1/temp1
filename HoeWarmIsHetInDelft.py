import requests
import re
# instead of parsing the whole page directly lets use the clientraw file directly same as its done by ajaxWDwx.js on the page

weer_url="https://weerindelft.nl/"
#path to json/ajax script
jsscriptDW="WU/ajaxWDwx.js"
# change debug to True for detailed exceptions
debug = False

# Extract name of client raw file from script in case it changes in the future 
try:
  r = requests.get(weer_url+jsscriptDW)
except Exception as e:
    if debug:
       print(e.args)
    print(f"Unable to get script from {weer_url+jsscriptDW} ")
    exit(1)
regex= "var clientrawFile\ = '\/([\w,.]*)'"
client_raw_file = re.search(regex, r.text).group(1)

# Get the raw file, extract temperature using seme method as ajaxWDwx.js and print rounded value
try:
  clientraw_page = requests.get(weer_url+client_raw_file)
except Exception as e:
    if debug:
       print(e.args)
    print(f"Unable to get raw file from {weer_url+client_raw_file} ")
    exit(1)

list2 = clientraw_page.text.split(' ')
#valid raw file has 12345 at start so lets valdiate before displaying 
if list2[0] == '12345':
    print(f"{round(float(list2[4]))} degrees Celcius \n")